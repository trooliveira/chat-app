# Documentação apos o fim do teste

## Tarefas Realizadas

1. Compilação de app escrito em go.
```
cd backend
go get ./... && go build
./backend --addr 0.0.0.0:8080 
```
A aplicação foi colocada para ser acessível por qualquer IP na porta 8080

> Incluso estes comandos para serem executados numa docker no arquivo **Dockerfile.go**

2. Compilação do frontend em React
`npm start`executado dentro do diretório frontend
> Incluso estes comandos para serem executados numa docker no arquivo **Dockerfile.react**

Ambos Dockerfiles estão incluso no docker-compose.yml para fazerem o build tendo ambos como um dependencia do outro.
A aplicação em GO tem dependencia do Redis, ao qual foi incluso também no **docker-compose.yml**

3. Para execução do build basta executar `docker-compose up -d --build`, na raiz da aplicação.

4. Foi criado um arquivo **.gitlab-ci.yml**, que serve para execução do pipeline automáticamente, quando se faz um push em alguma das branchs configuradas para tal, (develop e production)

5. Este pipeline no stage develop, realiza o build da aplicação. No stage production, realiza o build da aplicação e faz um push da imagem criada para o Dockerhub

> Para tal é necessário criar as variaveis descritas no **.gitlab-ci.yml** dentro do Gitlab, com os seus devidos valores

**Observações:**

1. Toda a parte de Ci/CD está funcional.
2. A imagem do redis não foi inclusa no push do deploy
3. O frontend, aparentemente, não conversa com o backend. Tentado verificar no código e corrigir este bug, mas não encontrei uma solução, somente onde se encontra o problema.

> O problema se encontra numa função anonima no arquivo **Chat.js** do frontend que, apesar de tudo corretamente configurado, não se conecta ao websocket do backend
