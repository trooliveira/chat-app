FROM golang:buster
RUN mkdir /app 
COPY ./backend /app
WORKDIR /app
RUN go get ./... && go build .
# EXPOSE 6379
CMD ["./backend", "--addr", "0.0.0.0:8080"]